package zenika.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import zenika.DTO.ShapeDTO;
import zenika.application.ShapeService;

@RestController
@RequestMapping("/api")
public class ShapeApiController {

    @Autowired
    private ShapeService shapeService;

    @GetMapping("/shapes/random")
    public ResponseEntity<ShapeDTO> getRandomShape(){
        ShapeDTO myShape = shapeService.getRandomShape();
        return ResponseEntity.ok(myShape);
    }

    @ExceptionHandler(value = IllegalArgumentException.class)
    public ResponseEntity urlNotValid(){
        return ResponseEntity.badRequest().body("Communication with color-api might not be working");
    }
}
